// Задание
// Написать реализацию кнопки "Показать пароль".
//
//     Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


let check = document.querySelectorAll('.icon-password');

document.querySelector('.fa-eye-slash').classList.add('fa-eye');
document.querySelector('.fa-eye-slash').classList.remove('fa-eye-slash');

for (let el of check){
    el.addEventListener('mousedown', () =>{
        el.classList.remove('fa-eye');
        el.classList.add('fa-eye-slash');

        const passField = el.parentElement.getElementsByTagName('input').item(0);
        passField.type = 'text';

    });

    el.addEventListener('mouseup', () => {
        el.classList.remove('fa-eye-slash');
        el.classList.add('fa-eye');

        const passField = el.parentElement.getElementsByTagName('input').item(0);
        passField.type = 'password';
    })
}

let btn = document.querySelector('.btn'),
    countError = 0;

btn.addEventListener('click', (e)=>{
    e.preventDefault();

    const pass = document.getElementById('pass').value,
        passRepeat = document.getElementById('passRepeat').value;


    if (pass === passRepeat && pass.length > 0) {
        alert('You are welcome');

    } else if (countError === 0) {
        const errorMsg = document.createElement('div');
        errorMsg.innerHTML = 'Нужно ввести одинаковые значения';
        errorMsg.style.color = 'red';

        let parentDiv = btn.parentNode;
        parentDiv.insertBefore(errorMsg,btn);
        countError++;
    }
});
